# -*- coding: utf-8 -*-
from django.core.validators import MinValueValidator
from django.db import models
from django.db.models.fields import CharField
import re
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext as _

class Aluno(models.Model):
    prontuario = models.CharField(max_length=30)
    nome = CharField(max_length=30)
    sobrenome = CharField(max_length=255)
    curso = CharField(max_length=2, default='0')
    ano = models.IntegerField()
    ativo = models.BooleanField(default=True)
    senha = CharField(max_length=30, blank=True)
    assistencia_estudantil = models.BooleanField(default=False)
    credito_refeicao = models.IntegerField(default=0)
    data_criacao = models.DateTimeField(u'Data de Criação', auto_now_add=True)

    def save(self, *args, **kwargs):
        # Verifica se o prontuário é inteiramente numérico
        if self.prontuario.isdigit():
            self.prontuario = str(int(self.prontuario))  # Converte para int e de volta para str para remover zeros à esquerda

        # Converte todo o prontuário para minúsculas
        self.prontuario = self.prontuario.lower()

        super(Aluno, self).save(*args, **kwargs)

class Search(models.Model):
    campo = CharField(max_length=255)

class UploadCsv(models.Model):
    titulo = models.CharField(max_length=100)
    curso = models.CharField(max_length=2, default='0')
    arquivo = models.FileField()
     
    data_criacao = models.DateTimeField(u'Data de Criação', auto_now_add=True)
        
