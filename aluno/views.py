# -*- coding: utf-8 -*-
from django.shortcuts import get_object_or_404, render, redirect
from django.template.context import RequestContext
from itertools import chain
from django.core.paginator import Paginator, EmptyPage
from django.contrib.auth.decorators import login_required, permission_required
from aluno.models import Aluno
from aluno.forms import AlunoForm, EditAlunoForm, SenhaForm
from curso.models import Curso
from horario.models import Horario
from .forms import UploadFileForm
import csv, xlrd

def paginador(request, alunos_list):
    paginator = Paginator(alunos_list, 10)
    page = request.GET.get('page',1)
    
    try:
        alunos = paginator.page(page)
    except EmptyPage:
        alunos = paginator.page(paginator.num_pages)
    except:
        alunos = paginator.page(1)
    
    return alunos

@login_required(login_url='/accounts/login/')
#@permission_required('aluno.add_aluno', raise_exception=True)
def alunos(request):
    alunos_list = Aluno.objects.filter(ativo=True)
    cursos = Curso.objects.all()
    logado =  False
    
    if request.user.is_active:
        logado =  True
        
    if request.method == "POST":
        campo = request.POST.get('campo')
        ano = request.POST.get('ano')
        curso = request.POST.get('curso')
        search = montaLista(campo, ano, curso)
        
        csrfContext = RequestContext(request)
        
        if search:
            return render(request, 'aluno/alunos.html', {'alunos': search, 'registros': search.__len__(), 'cursos': cursos, 'logado': logado}, csrfContext)
        else:
            err = "Nenhum registro encontrado"
            alunos = paginador(request, alunos_list)
            return render(request, 'aluno/alunos.html', {'alunos': alunos, 'err': err, 'cursos': cursos, 'logado': logado}, csrfContext)
    else:
        alunos = paginador(request, alunos_list)
        
        if '0' in request.session:
            msg = request.session['0']
            del request.session['0']
            return render(request, 'aluno/alunos.html', {'alunos': alunos, 'msg': msg, 'cursos': cursos, 'logado': logado})
        
        return render(request, 'aluno/alunos.html', {'alunos': alunos, 'cursos': cursos, 'logado': logado})

def montaLista(campo, ano, curso):
    aux = []
        
    if campo == u"":
        nome = Aluno.objects.filter(nome__icontains=campo, ativo=True)
        sobrenome = Aluno.objects.filter(sobrenome__icontains=campo, ativo=True)
        prontuario = Aluno.objects.filter(prontuario__icontains=campo, ativo=True)
            
        search = list(chain(nome, sobrenome,prontuario))
        aux = list(set(search))
    else:
        campos = campo.split()
        for x in campos:
            nome = Aluno.objects.filter(nome__icontains=x, ativo=True)
            sobrenome = Aluno.objects.filter(sobrenome__icontains=x, ativo=True)
            prontuario = Aluno.objects.filter(prontuario__icontains=x, ativo=True)
                
            search = list(chain(nome, sobrenome,prontuario))
            search = list(set(search))
            aux = list(chain(search, aux))
    
    if ano != u'0':
        ano = list(Aluno.objects.filter(ano=ano, ativo=True))
        ids_ano = set(aluno.prontuario for aluno in ano)
        aux = [item for item in aux if item.prontuario in ids_ano]
        
    if curso != u'0':
        curso = list(Aluno.objects.filter(curso=curso, ativo=True))
        ids_curso = set(aluno.prontuario for aluno in curso)
        aux = [item for item in aux if item.prontuario in ids_curso]
        
    return aux     

@login_required(login_url='/accounts/login/')
#@permission_required('aluno.delete_aluno',raise_exception=True)
def deletarAluno_confirm(request, id):
    logado =  False
    
    if request.user.is_active:
        logado =  True
        
    qst = Aluno.objects.filter(id=id, ativo=True)
    for i in qst:
        aluno = i
    return render(request, 'delete.html', {'aluno': aluno, 'app': 'Aluno', 'logado': logado})

@login_required(login_url='/accounts/login/')
#@permission_required('aluno.delete_aluno',raise_exception=True)
def deletarAluno(request, id):
    obj = get_object_or_404(Aluno, pk = id)
    obj.ativo = False
    obj.save()
    
    obj = get_object_or_404(Horario, aluno_id = id)
    obj.delete()    
    
    request.session[0] = "Aluno deletado com sucesso"
    return redirect("/alunos")

@login_required(login_url='/accounts/login/')
#@permission_required('aluno.add_aluno',raise_exception=True)
def addAluno(request):
    cursos = Curso.objects.all()
    logado =  False
    
    if request.user.is_active:
        logado =  True
    if request.method == "POST":
        form = AlunoForm(request.POST)
        if form.is_valid():
            assistencia_estudantil = request.POST.get("assistencia_estudantil")

            assistencia_estudantil = True if assistencia_estudantil == "on" else False

            aluno = Aluno(
                nome = request.POST.get("nome"),
                sobrenome = request.POST.get("sobrenome"),
                prontuario  = request.POST.get("prontuario"),
                curso = request.POST.get("curso"),
                ano = request.POST.get("ano"),
                assistencia_estudantil = assistencia_estudantil
            )
            
            aluno.save()
            
            request.session[0] = "Aluno inserido com sucesso"

            return redirect("/alunos")
        else:
            alunos = Aluno.objects.filter(ativo=True)
            alunos = paginador(request, alunos)
            return render(request, 'aluno/add_aluno.html', {'form': form, 'cursos': cursos, 'logado': logado})
    else:
        form = AlunoForm
        return render(request, 'aluno/add_aluno.html', {'form': form, 'cursos': cursos, 'logado': logado})

@login_required(login_url='/accounts/login/')
#@permission_required('aluno.change_aluno',raise_exception=True)
def editarAluno(request, id):
    aluno = get_object_or_404(Aluno, pk = id)
    cursos = Curso.objects.all()
    logado =  False
    
    if request.user.is_active:
        logado =  True
    if request.method == 'POST':
        form = EditAlunoForm(request.POST, initial={'id': id})
        if form.is_valid():
            aluno.nome = request.POST.get("nome")
            aluno.sobrenome = request.POST.get("sobrenome")
            aluno.prontuario  = request.POST.get("prontuario")
            aluno.curso = request.POST.get("curso")
            aluno.ano = request.POST.get("ano")
            aluno.assistencia_estudantil = request.POST.get("assistencia_estudantil") == "on"
        
            aluno.save()
            
            request.session[0] = "Aluno alterado com sucesso"
            
            return redirect("/alunos")              
        else:
            return render(request, 'aluno/editar.html', {'form': form,'cursos': cursos, 'logado': logado})
    else:
        form = EditAlunoForm(initial={
            'nome': aluno.nome,
            'sobrenome': aluno.sobrenome,
            'prontuario': aluno.prontuario,
	        'curso': aluno.curso,	
	        'ano': str(aluno.ano),
            'assistencia_estudantil': aluno.assistencia_estudantil,

	    })
   
        return render(request, 'aluno/editar.html', {'form': form,'cursos': cursos, 'logado': logado})

@login_required(login_url='/accounts/login/')
#@permission_required('aluno.change_aluno',raise_exception=True)
def editarLote(request):
    cursos = Curso.objects.all()
    logado =  False
    
    if request.user.is_active:
        logado =  True
    if request.method == 'POST':
        action = request.POST.get('button')
        if action is None:
            ano = request.POST.get('ano')
            curso = request.POST.get('curso')
        
            if ano != u'0' or curso != u'0':
                search = montaLista(u'', ano, curso)
                return render(request, 'aluno/editar_lote.html', {'search': search, 'cursos':cursos, 'logado': logado})
            else:
                search = None;
                return render(request, 'aluno/editar_lote.html', {'search': search, 'err': "Nenhum registro encontrado", 'cursos':cursos, 'logado': logado})
        else:
            if action == u"Alterar":
                ano = request.POST.get('editano')
                lista = request.POST.getlist('lista') 
                
                if ano == u"0":
                    return render(request, 'aluno/editar_lote.html', {'err': "O ano para alteração não foi selecionado", 'cursos':cursos, 'logado': logado})
                if len(lista) == 0:
                    return render(request, 'aluno/editar_lote.html', {'err': "Nenhum aluno foi selecionado", 'cursos':cursos, 'logado': logado})
                
                for registro in lista:
                    aluno = get_object_or_404(Aluno, pk = registro)
                    aluno.ano = ano
                    aluno.save()
                
                msg = "Alteração feita com sucesso"                
            else:
                lista = request.POST.getlist('lista')
                
                if len(lista) == 0:
                    return render(request, 'aluno/editar_lote.html', {'err': "Nenhum aluno foi selecionado", 'cursos':cursos, 'logado': logado})
                
                for registro in lista:
                    aluno = get_object_or_404(Aluno, pk = registro)
                    aluno.ativo = False
                    aluno.save()
                
                msg = "Exclusão feita com sucesso"
            return render(request, 'aluno/editar_lote.html', {'msg': msg, 'cursos':cursos, 'logado': logado})                
    else:
        return render(request, 'aluno/editar_lote.html', {'cursos':cursos, 'logado': logado})

def uploadXls(request):
    cursos = Curso.objects.all()
    logado =  False
    
    if request.user.is_active:
        logado =  True
    
    if request.method == 'POST':
        arquivo = request.FILES['arquivo']  
        form = UploadFileForm(request.POST, initial={'arquivo': arquivo})
    
        if form.is_valid():
            curso = request.POST.get("curso") 
            ano = int(request.POST.get("ano"))
            prontuarioIndex = None
            resultado = None
            nome = None
            msg = None
            falhas = []
            controle = salvos = total = 0
            rel = ""
            
            arquivo = xlrd.open_workbook(file_contents=arquivo.read(), encoding_override="UTF-8")
            folha = arquivo.sheet_by_index(0)
            
            if folha.cell(0,0).value == "":
                controle = 1
            
            for x in range(0,folha.ncols):
                if folha.cell(controle,x).value == "Aluno" or folha.cell(controle,x).value == "Nome":
                    nome = x
                if folha.cell(controle,x).value == "Matrícula".decode('UTF-8'):
                    prontuarioIndex = x
                if folha.cell(controle,x).value == "Situação no Período".decode('UTF-8') or folha.cell(controle,x).value == "Situação no Curso".decode('UTF-8'):
                    resultado = x
                                    
                if nome != None and prontuarioIndex != None and resultado != None:
                    break;
                
            controle += 1
            
            for x in range(controle, folha.nrows):
                total += 1
                aux = folha.cell(x,nome).value.split(" ")
                first =  True
                sobrenome = ""
                
                for item in aux:
                    item.capitalize()
                    
                    if first:
                        first = False
                        firstName = item
                    else:
                        sobrenome += item+" "
                
                lastName = sobrenome
                
                
                #firstName = folha.cell(x,nome).value.split(" ")[0].capitalize()
                #lastName = folha.cell(x,nome).value[(len(firstName)+1):]
                prontuario = folha.cell(x,prontuarioIndex).value
                
                aluno = Aluno.objects.filter(prontuario=prontuario)
                
                if folha.cell(x,resultado).value == "Matriculado" and not aluno.exists():
                    aluno = Aluno(
                        prontuario = prontuario,
                        nome = firstName,
                        sobrenome = lastName,
                        curso = curso,
                        ano = ano
                    )
                    
                    aluno.save()
                    salvos += 1
                    
                    if rel == "":
                        rel = aluno.prontuario
                    else:
                        rel += ","+aluno.prontuario
                        
                elif folha.cell(x,resultado).value != "Matriculado":
                    falhas.append([prontuario, firstName, lastName, "Aluno não matriculado neste periodo."])
                else:    
                    falhas.append([prontuario, firstName, lastName, "Pnrotuario previamente cadastrado."])
            
            if not falhas and salvos != 0:
                msg = [True, "Todos os alunos foram inseridos corretamente, foram adicionados "+str(salvos)+" alunos"]
            elif salvos == 0:
                form.add_error(None, "Nenhum registro de aluno foi salvo")
            else:
                msg = [False, ("Alguns registros não foram salvos, foram gravados "+str(salvos)+" de "+str(total))]
                
            if len(falhas) == 0:
                request.session[0] = str(salvos)+" Alunos inseridos"
                return redirect('/alunos')
            else:
                return render(request, 'aluno/uploadXLS.html', {'form': form,'cursos': cursos, 'falhas': falhas, 'salvos': salvos, 'total': salvos+len(falhas), 'err': "Alguns registros não foram salvos", 'logado': logado})
        else:
            return render(request, 'aluno/uploadXLS.html', {'form': form,'cursos': cursos, 'logado': logado})
    else:        
        return render(request, 'aluno/uploadXLS.html', {'cursos': cursos, 'logado': logado})

@login_required(login_url='/accounts/login/')
#@permission_required('aluno.add_aluno', raise_exception=True)
def uploadCsv(request):
    cursos = Curso.objects.all()
    logado =  False
    
    if request.user.is_active:
        logado =  True
    if request.method == 'POST':
        arquivo = request.FILES['arquivo']  
        form = UploadFileForm(request.POST, initial={'arquivo': arquivo})
        
        if form.is_valid():
            curso = int(request.POST.get("curso")) 
            ano = int(request.POST.get("ano"))
            prontuarioIndex = None
            resultado = None
            nome = None
            falhas = []
            salvos = 0
            
            reader = csv.reader(arquivo)
            
            for row in reader:

                colunas = row[0].split(";")
                
                if colunas[0] != "":
                    index = 0
                    for item in colunas:
                        if "nome" in item.lower():
                            nome = index
                        if "matrícula" in item.lower():
                            prontuarioIndex = index
                        if "situação no curso" in item.lower():
                            resultado = index
                        
                        index += 1
                
                if nome != None and prontuarioIndex != None and resultado != None:
                    break;        
                
            for row in reader:
                colunas = row[0].split(";")
                fullname = []
                prontuario = colunas[prontuarioIndex]
                
                if prontuario.startswith("PE"):
                    prontuario = prontuario[2:]
                
                aluno = Aluno.objects.filter(prontuario=prontuario)
                fullname.append(colunas[nome].split(" ")[0])
                primeira = len(fullname[0]) + 1
                fullname.append(colunas[nome][primeira:])
                firstName = fullname[0]
                lastName = fullname[1]
                
                if colunas[resultado].lower() == "matriculado":
                    if not aluno.exists():
                        aluno = Aluno(
                            nome = firstName,
                            sobrenome = lastName,
                            prontuario  = prontuario,
                            curso = curso,
                            ano = ano
                        )
                        
                        aluno.save()
                        salvos += 1
                    else:
                        falhas.append([firstName, lastName, colunas[prontuarioIndex], 'Prontuario já Cadastrado'])    
                else:
                    falhas.append([firstName, lastName, colunas[prontuarioIndex], 'Aluno Reprovado no Ano Anterior'])
                    
            if len(falhas) == 0:
                request.session[0] = str(salvos)+" Alunos inseridos"
                return redirect('/alunos')
            else:
                return render(request, 'aluno/uploadCSV.html', {'form': form,'cursos': cursos, 'falhas': falhas, 'salvos': salvos, 'total': salvos+len(falhas), 'err': "Alguns registros não foram salvos", 'logado': logado})
        else:
            return render(request, 'aluno/uploadCSV.html', {'form': form,'cursos': cursos, 'logado': logado})
    else:
        return render(request, 'aluno/uploadCSV.html', {'cursos': cursos, 'logado': logado})

@login_required(login_url='/accounts/login/')
#@permission_required('aluno.change_aluno',raise_exception=True)
def editarSenha(request, id):
    aluno = get_object_or_404(Aluno, pk = id)
    logado =  False
    
    if request.user.is_active:
        logado =  True
    if request.method == 'POST':
        form = SenhaForm(request.POST, initial={'id': id})
        if form.is_valid():
            aluno.senha = request.POST.get("senha")
        
            aluno.save()
            
            request.session[0] = "Senha alterado com sucesso"
            
            return redirect("/alunos")              
        else:
            return render(request, 'aluno/senha.html', {'form': form,'logado': logado})
    else:
        form = SenhaForm()
        senha = aluno.senha
        
        if aluno.senha == u' ':
            senha = None
        else:
            senha = aluno.senha
        
        return render(request, 'aluno/senha.html', {'form': form, 'senha': senha, 'logado': logado})
