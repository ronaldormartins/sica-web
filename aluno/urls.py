# -*- coding: utf-8 -*-

from django.conf.urls import url
from aluno import  views

urlpatterns = [
    url(r'^$', views.alunos),
    url(r'^delete/(?P<id>[0-9]+)/$', views.deletarAluno_confirm, name='deletar_aluno'),
    url(r'^delete/pos_confirm/(?P<id>[0-9]+)/$', views.deletarAluno, name='deletar_aluno_posconfirm'),
    url(r'^add', views.addAluno),
    url(r'^edit/(?P<id>[0-9]+)/$', views.editarAluno, name='editar_aluno'),
    url(r'^editSenha/(?P<id>[0-9]+)/$', views.editarSenha, name='editar_senha'),
    url(r'^editlote', views.editarLote, name='editar_lote'),
    #url(r'^uploadCSV', views.uploadCsv, name='upload_csv'),
    url(r'^uploadXLS', views.uploadXls, name='upload_xls'),
]