# -*- coding: utf-8 -*-

from django.db import models

class CodigoSegurancaApi(models.Model):
    sistema = models.TextField(max_length=100)
    codigo = models.CharField(max_length=10, default="Inválido", unique=True)
    data = models.DateField(u'Data de Criação', auto_now_add=True)