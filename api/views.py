# -*- coding: utf-8 -*-

from django.contrib.auth.decorators import login_required, permission_required
from django.views.decorators.csrf import csrf_exempt
from django.http.response import JsonResponse
from django.shortcuts import render, redirect
from relatorio.models import Ticket
from aluno.models import Aluno
from api.models import CodigoSegurancaApi
import hashlib, time

def createHash():
    codigo = hashlib.sha1()
    codigo.update(str(time.time()))
    return codigo.hexdigest()[:10] 

@login_required(login_url='/accounts/login/')
@permission_required('api.add_api', raise_exception=True)
def inicial(request):
    sistemas = CodigoSegurancaApi.objects.all()
    logado =  False
    
    if request.user.is_active:
        logado =  True
    
    if request.method == "POST":
        controle = 0
        
        while controle != 1:
            codigoHash = createHash()
            
            instancias = CodigoSegurancaApi.objects.filter(codigo=codigoHash).count()
            
            if instancias == 0:
                controle = 1
             
        codigo = CodigoSegurancaApi(
            sistema = request.POST.get("sistema"),
            codigo = codigoHash
        )
        
        codigo.save()
        
        return render(request, 'api/api.html', {'sistemas': sistemas, 'msg': 'Código de Acesso gerado com sucesso', 'logado': logado})
    else:
        if '0' in request.session:
            msg = request.session['0']
            del request.session['0']
            
            return render(request, 'api/api.html', {'sistemas': sistemas, 'logado': logado, 'msg': msg})
        
        if '1' in request.session:
            msg = request.session['1']
            del request.session['1']
            
            return render(request, 'api/api.html', {'sistemas': sistemas, 'logado': logado, 'err': msg})
        
        return render(request, 'api/api.html', {'sistemas': sistemas, 'logado': logado})

@login_required(login_url='/accounts/login/')
@permission_required('api.add_api', raise_exception=True)
def alterar(request):
    if request.method == "POST":
        sistema = request.POST.get("sistema")
        identificador = request.POST.get("id")
        
        codigo = CodigoSegurancaApi.objects.filter(id=identificador)
    
        if codigo.count() == 1:
            codigo = codigo.first()
            
        controle = 0
        
        while controle != 1:
            codigoHash = createHash()
            
            instancias = CodigoSegurancaApi.objects.filter(codigo=codigoHash).count()
            
            if instancias == 0:
                controle = 1
            
        codigo.sistema = sistema
        codigo.codigo = codigoHash
        
        codigo.save()
        
        request.session[0] = "Chave de Integração Atualizada com sucesso"
        return redirect("/api")
    else:
        request.session[1] = "Erro de acesso a página"
        return redirect("/api")

@login_required(login_url='/accounts/login/')
@permission_required('api.add_api', raise_exception=True)
def deletar(request, id):
    codigo = CodigoSegurancaApi.objects.filter(id=id)
    
    if codigo.count() == 1:
        codigo = codigo.first()
        codigo.delete()
        
    request.session[0] = "Vinculo de Sistema deletado com sucesso"
    
    return redirect("/api")

def buscaAluno(prontuario):
    if u'PE' in prontuario.upper() and len(prontuario) == 9:
        prontuario = prontuario[2:]            
        
    aluno = Aluno.objects.filter(prontuario__icontains=prontuario, ativo=True)
    
    return aluno

def ticketDIC(tickets):
    dicionario = {}
    aux = 0
    
    for ticket in tickets:
        prontuario = ticket.aluno.prontuario
        data = dict(year=ticket.data.day, month=ticket.data.month, day=ticket.data.year)
        tipo = ticket.tipo
        
        dicionario.update({aux:{"prontuario": prontuario, "data": data, "tipo": tipo}})
        aux+=1
        
    return dicionario

@csrf_exempt
def geraTicketAPI(request):
    if request.method == 'POST':
        prontuario = request.POST.get("prontuario")
        data = request.POST.get("data")
        chave = request.POST.get("chave")
        
        codigo = CodigoSegurancaApi.objects.filter(codigo=chave)
        
        if codigo.count() != 0:
            aluno = buscaAluno(prontuario)
            
            if aluno.count() == 1:
                aluno = aluno.first()
                ticket = Ticket.objects.filter(aluno__id__icontains=aluno.id, data__icontains=data)
                    
                if ticket.count() == 0:
                    ticket = Ticket(
                        aluno = aluno,
                        data = data,
                        tipo = 2,
                    )
                        
                    ticket.save()
                    
                    return JsonResponse({'status': 1, 'msg': 'Ticket Salvo com Sucesso'})
                else:
                    return JsonResponse({'status': 0, 'msg': 'Ticket Gerado Anteriormente'})
            else:
                return JsonResponse({'status': 0, 'msg': 'Nenhum Aluno Encontrado'})
        else:
            return JsonResponse({'status': 0, 'msg': 'Chave de Integração Incorreta'})
    else:
        return JsonResponse({'status': 0, 'msg': 'Erro de Requisição'})
                
@csrf_exempt
def consultaTicketAPI(request):
    if request.method == 'POST':
        prontuario = request.POST.get("prontuario")
        chave = request.POST.get("chave")
        
        codigo = CodigoSegurancaApi.objects.filter(codigo=chave)
        
        if codigo.count() != 0:
            aluno = buscaAluno(prontuario)
            
            if aluno.count() == 1:
                aluno = aluno.first()
                tickets = list(Ticket.objects.filter(aluno__id__icontains=aluno.id, tipo=0))
                
                return JsonResponse({'status': 1, 'tickets': ticketDIC(tickets)})
            else:
                return JsonResponse({'status': 0, 'msg': 'Nenhum Aluno Encontrado'})
        else:
            return JsonResponse({'status': 0, 'msg': 'Chave de Integração Incorreta'})
    else:
        return JsonResponse({'status': 0, 'msg': 'Erro de Requisição'})

@csrf_exempt
def deletaTicketAPI(request):
    if request.method == 'POST':
        prontuario = request.POST.get("prontuario")
        data = request.POST.get("data")
        chave = request.POST.get("chave")
        
        codigo = CodigoSegurancaApi.objects.filter(codigo=chave)
        
        if codigo.count() != 0:
            aluno = buscaAluno(prontuario)
            
            if aluno.count() == 1:
                aluno = aluno.first()
                
                ticket = Ticket.objects.raw("SELECT * FROM relatorio_ticket where data = TO_DATE('"+data+"', 'YYYY-MM-DD') AND aluno_id = "+str(aluno.id))
                ticket = list(ticket)
                
                if len(ticket) == 1:
                    ticket = ticket[0]
                else:
                    return ({'status': 0, 'msg': 'Há mais de um ticket gerado para este aluno para a data informada'})
            
                ticket.delete()
                
                return JsonResponse({'status': 1, 'msg': 'Data do Ticket Deletado com Sucesso'})
        else:
            return JsonResponse({'status': 0, 'msg': 'Chave de Integração Incorreta'})
    else:
        return JsonResponse({'status': 0, 'msg': 'Erro de Requisição'})

@csrf_exempt    
def editaTicketAPI(request):
    if request.method == 'POST':
        prontuario = request.POST.get("prontuario")
        dataAtual = request.POST.get("data")
        dataNova = request.POST.get("dataNova")
        chave = request.POST.get("chave")
        
        codigo = CodigoSegurancaApi.objects.filter(codigo=chave)
        
        if codigo.count() != 0:
            aluno = buscaAluno(prontuario)
            
            if aluno.count() == 1:
                aluno = aluno.first()
                
                ticket = Ticket.objects.raw("SELECT * FROM relatorio_ticket where data = TO_DATE('"+dataAtual+"', 'YYYY-MM-DD') AND aluno_id = "+str(aluno.id))
                aux = Ticket.objects.raw("SELECT * FROM relatorio_ticket where data = TO_DATE('"+dataNova+"', 'YYYY-MM-DD') AND aluno_id = "+str(aluno.id))
                
                ticket = list(ticket)
                aux = list(aux)
                
                if len(ticket) == 1:
                    ticket = ticket[0]
                else:
                    return ({'status': 0, 'msg': 'Há mais de um ticket gerado para este aluno para a data informada'})
                
                if len(aux) != 0:
                    return ({'status': 0, 'msg': 'Um ticket já foi gerado para o aluno na data informada'})
                
                ticket.data = dataNova
                
                ticket.save()
                
                return JsonResponse({'status': 1, 'msg': 'Data do Ticket Editado com Sucesso'})
            else:
                return JsonResponse({'status': 1, 'msg': 'Mais de um aluno foi encontrado'})
        else:
            return JsonResponse({'status': 0, 'msg': 'Chave de Integração Incorreta'})
    else:
        return JsonResponse({'status': 0, 'msg': 'Erro de Requisição'})