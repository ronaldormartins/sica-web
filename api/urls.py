# -*- coding: utf-8 -*-

from django.conf.urls import url
from api import  views

urlpatterns = [
    url(r'^ticket', views.geraTicketAPI),
    url(r'^consultar', views.consultaTicketAPI),
    url(r'^editar', views.editaTicketAPI),
    url(r'^excluir', views.deletaTicketAPI),
    url(r'^$', views.inicial),
    url(r'^edit', views.alterar),
    url(r'^delete/(?P<id>[0-9]+)/$', views.deletar, name='deletar_api'), 
]