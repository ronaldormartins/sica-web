# -*- coding: utf-8 -*-

from django.contrib.auth.forms import AuthenticationForm 
from django import forms
from core.models import Cardapio
from captcha.fields import ReCaptchaField
from captcha.widgets import ReCaptchaV2Checkbox

class GerarTicket(forms.Form):
    captcha = ReCaptchaField(widget=ReCaptchaV2Checkbox)

class SenhaGerarTicket(forms.Form):
    senha = forms.CharField(widget=forms.PasswordInput, label="Senha")
    captcha = ReCaptchaField(widget=ReCaptchaV2Checkbox)

class LoginForm(AuthenticationForm):
    username = forms.CharField(label="Username", max_length=30, 
                               widget=forms.TextInput(attrs={'class': 'form-control', 'name': 'username'}))
    password = forms.CharField(label="Password", max_length=30, 
                               widget=forms.PasswordInput(attrs={'class': 'form-control', 'name': 'password'}))

class CardapioForm(forms.ModelForm):
    
    class Meta:
        model = Cardapio
        fields = ('data','principal','salada','sobremesa', 'suco')
    
    def clean(self):
        super(CardapioForm,self).clean()
        
        data = self.cleaned_data.get('data')
        
        cardapio = Cardapio.objects.filter(data=data)
        
        if self.errors:
            return self.cleaned_data
        
        if cardapio:
            self.add_error('data', u'A data selecionada já tem um cardápio registrado')
        
        return self.cleaned_data

class EditarCardapioForm(forms.ModelForm):
    
    class Meta:
        model = Cardapio
        fields = ('data','principal','salada','sobremesa', 'suco')
    
    def clean(self):
        super(EditarCardapioForm,self).clean()
        data = self.cleaned_data.get('data')
        id = self.initial.get('id')
        
        cardapio_new = Cardapio.objects.filter(data=data)
        cardapio_old = Cardapio.objects.filter(id=id)
        
        if self.errors:
            return self.cleaned_data
        
        if cardapio_new:
            cardapio_new = cardapio_new[0]
            if cardapio_old:
                cardapio_old = cardapio_old[0]
                if cardapio_new.data != cardapio_old.data:
                    self.add_error('data', u'A data selecionada já tem um cardápio registrado')
        
        return self.cleaned_data

            