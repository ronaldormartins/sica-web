# -*- coding: utf-8 -*-

from django.contrib import admin
from core.models import Cardapio

class CardapioAdmin(admin.ModelAdmin):
    list_display = ('id','data')
    ordering = ('id','data')
    search_fields = ('id','data')
    list_display_icons = True
    export_to_xls = True
    
admin.site.register(Cardapio, CardapioAdmin)