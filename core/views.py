# -*- coding: utf-8 -*-

from django.template import RequestContext
from django.contrib.auth import authenticate, logout, login as auth_login, get_user_model
from django.shortcuts import redirect, render, get_object_or_404
from django.contrib.auth.decorators import login_required, permission_required
from core.models import Cardapio
from aluno.views import paginador, Aluno
from core.forms import CardapioForm, EditarCardapioForm
from curso.models import Curso
from horario.models import Horario
from relatorio.models import Ticket
from creditos.models import Historico
import datetime
from core.forms import SenhaGerarTicket, GerarTicket



def accountsLogin(request):
    if request.user.is_active:
        return home(request)
    else:
        if '0' in request.session:
            msg = request.session['0']
            del request.session['0']
        else:
            msg = "Login Necessário"
        return render(request, 'index.html', {'msg_err': msg})


def accountsPassword(request):
    if request.method == 'POST':
        usuario = request.POST.get("form-username")
        oldPassword = request.POST.get("form-oldpassword")
        newPassword = request.POST.get("form-newpassword")

        if oldPassword == newPassword:
            return render(request, 'password.html', {'msg_err': "As senhas não podem ser iguais"})

        user = authenticate(username=usuario, password=oldPassword)

        if user is not None:
            user.set_password(newPassword)
            user.save()

            request.session['0'] = "A senha foi redefinida com sucesso."

            return redirect('/accounts/login/')
        else:
            return render(request, 'password.html', {'msg_err': "Usuário ou senha inválidos"})
    else:
        return render(request, 'password.html')


def login(request):
    if request.user.is_active:
        return home(request)
    else:
        return render(request, 'index.html')


def sica_login(request):
    username = request.POST['form-username']
    password = request.POST['form-password']
    user = authenticate(username=username, password=password)

    msg = []

    if user is not None:
        if user.is_active:
            auth_login(request, user)
            return redirect('/home')
        else:
            msg.append("login inativo")
    else:
        msg.append("Usuário ou senha inválidos")

    return render(request, 'index.html', {'errors': msg})


def sica_logout(request):
    logout(request)
    return redirect('/')


def recuperar_cardapios_e_datas():
    data = datetime.datetime.now().date()
    lstData = []
    lstCardapio = []

    for x in range(0, 4):
        if (x == 0):
            qst = Cardapio.objects.filter(data=data)
        else:
            data = data + datetime.timedelta(days=1)

            if data.weekday() == 5:
                data = data + datetime.timedelta(days=2)

            qst = Cardapio.objects.filter(data=data)

        lstData.append(data)

        if qst:
            for i in qst:
                cardapio = i
            lstCardapio.append(cardapio)
        else:
            lstCardapio.append(qst)

    return data, lstData, lstCardapio


def home(request):
    data, lstData, lstCardapio = recuperar_cardapios_e_datas()
    logado = request.user.is_active


    if request.method == 'POST':
        # Gerar ticket

        # region obtem informações da requisição
        csrfContext = RequestContext(request)
        prontuario = request.POST.get("prontuario")
        senha = request.POST.get("senha")
        #endregion

        # region Verifica se está dentro do horário para gerar ticket
        data = datetime.datetime.now()
        dMin = data.replace(hour=6, minute=40)
        dMax = data.replace(hour=12, minute=00)
        if not (data >= dMin and data <= dMax):
            erro = "Fora do Horário para emissão de Ticket"
            return render(request, 'home.html',
                          {'cardapio': lstCardapio, 'data': lstData, 'erro': erro, 'logado': logado,
                           'form': GerarTicket(request.POST)}, csrfContext)
        # endregion

        # region Ajusta data para a próxima refeição
        data = data.date()
        # Verifica se o dia da semana é sexta-feira (weekday() == 4)
        if data.weekday() == 4:
            # Se for sexta-feira, adiciona 3 dias para pular o fim de semana (sábado e domingo e ir para a próxima segunda-feira
            data = data + datetime.timedelta(days=3)
        else:
            # Se não for sexta-feira, simplesmente avança para o próximo dia
            data = data + datetime.timedelta(days=1)
        # endregion

        #region Busca aluno por prontuario
        if u'ct' in prontuario.lower() and len(prontuario) == 9:
            prontuario = prontuario[2:]
        aluno = Aluno.objects.filter(prontuario=prontuario.lower(), ativo=True)
        if aluno.count() == 0:
            erro = "Prontuário Inválido"
            return render(request, 'home.html',
                          {'cardapio': lstCardapio, 'data': lstData, 'erro': erro, 'logado': logado,
                           'form': GerarTicket(request.POST)}, csrfContext)
        else:
            aluno = aluno.first()
        #endregion

        #region Valida Captcha
        act = request.POST.get("act")
        if (act == 'valid_pass'):
            form = SenhaGerarTicket(request.POST)
            if not form.is_valid():
                erro =  'Erro de validação do reCAPTCHA. Por favor, tente novamente.'
                return render(request, 'senha_gerar_ticket.html',
                              {'form': form, 'aluno': aluno.first(), 'logado': logado,
                               'erro':erro, 'form':form}, csrfContext)
        else:
            form = GerarTicket(request.POST)
            if not form.is_valid():
                erro = 'Erro de validação do reCAPTCHA. Por favor, tente novamente.'
                return render(request, 'home.html',
                              {'cardapio': lstCardapio, 'data': lstData, 'erro': erro, 'logado': logado,
                               'form': GerarTicket(request.POST)},csrfContext)
        #endregion

        # region Verifica se ticket já foi gerado anteriormente
        ticket = Ticket.objects.filter(aluno__id=aluno.id, data__icontains=data)
        if ticket.count() != 0:
            erro = "Gerado anteriormente"
            return render(request, 'home.html',
                          {'cardapio': lstCardapio, 'data': lstData, 'erro': erro, 'logado': logado,
                           'form': GerarTicket(request.POST)},
                          csrfContext)
        #endregion

        # region Verifica se é uma data válida
        horario = Horario.objects.filter(aluno__id__icontains=str(aluno.id)).first()
        if data.weekday() == 0:
            valor = horario.seg
        elif data.weekday() == 1:
            valor = horario.ter
        elif data.weekday() == 2:
            valor = horario.qua
        elif data.weekday() == 3:
            valor = horario.qui
        elif data.weekday() == 4:
            valor = horario.sex
        else:
            erro = "Final de Semana"
            return render(request, 'home.html',
                         {'cardapio': lstCardapio, 'data': lstData, 'erro': erro, 'logado': logado,
                          'form':GerarTicket(request.POST)}, csrfContext)

        # Verifica se o objeto horário está atribuido valor parao dia especificado
        if not valor:
            erro = "Dia inválido - Aluno não liberado"
            return render(request, 'home.html',
                          {'cardapio': lstCardapio, 'data': lstData, 'erro': erro,
                           'logado': logado, 'form':GerarTicket(request.POST)}, csrfContext)
        #endregion

        #  region Valida Senha
        # A senha que veio pela requisição é vazia e existe senha no aluno?
        # Ou seja, deve ser informado senha?
        if senha == None and aluno.senha != u"":
            form = SenhaGerarTicket(request.POST)
            return render(request, 'senha_gerar_ticket.html',
                         {'form':form, 'aluno': aluno, 'logado': logado}, csrfContext)
        elif aluno.senha != u"":
            # Senha é incorreta?
            if senha != aluno.senha:
                return render(request, 'home.html',
                              {'cardapio': lstCardapio, 'data': lstData, 'erro': "Senha Incorreta",
                               'logado': logado, 'form':GerarTicket(request.POST)}, csrfContext)
        #endregion

        #region verifica cota

        #Variaveis de controle
        isCredito = False

        #Recupera a cota do curso do aluno


        curso = Curso.objects.filter(id=int(aluno.curso), ativo=True).first()
        cota = curso.cota
        #Se possui cota, provavelmente é do superior, precisa verificar se possui cota ou crédito
        if cota <> None:
            ids_cursos = Curso.objects.filter(cota__id=cota.id).values_list('id', flat=True)
            ids_cursos_str = [str(id_curso) for id_curso in ids_cursos]

            quantidade_tickets = Ticket.objects.filter(
                aluno__curso__in=ids_cursos_str,
                data__date=data
            ).count()

            #Verifica se tem cota suficiente
            if cota.quantidade > quantidade_tickets:
                if not aluno.assistencia_estudantil:
                    #Verifica se possui credito
                    if aluno.credito_refeicao > 0:
                        isCredito = True
                    else:
                        erro = "Não possui crédito ou assistênca estudantil"
                        return render(request, 'home.html',
                                     {'cardapio': lstCardapio, 'data': lstData, 'erro': erro, 'logado': logado,
                                      'form': GerarTicket(request.POST)}, csrfContext)
            else:
                #Não possui cota suficiente
                erro = "Curso não possui cota suficiente"
                return render(request, 'home.html',
                              {'cardapio': lstCardapio, 'data': lstData, 'erro': erro, 'logado': logado,
                               'form': GerarTicket(request.POST)}, csrfContext)
        #endregion

        # region Gera ticket
        #Se chegou até aqui é porque pode ger ticket
        ticket = Ticket(
            aluno=aluno,
            data=data,
            tipo=1,
        )
        ticket.save()

        #Se gerou ticket por crédito, precisa decrementar o crédito
        if isCredito:
            aluno.credito_refeicao -= 1
            aluno.save()

            user = request.user

            historico = Historico(
                justificativa='Crédito consumido',
                usuario=user,
                aluno=aluno,
                unidades=-1
            )
            historico.save()

        return render(request, 'home.html',
                      {'cardapio': lstCardapio, 'data': lstData, 'msg': 'ok', 'logado': logado,
                       'form':GerarTicket(request.POST)}, csrfContext)

        #endregion

    else:
        # Apenas carregar página
        return render(request, 'home.html', {'cardapio': lstCardapio, 'data': lstData, 'logado': logado, 'form':GerarTicket(request.POST)})


def formata_data(s):
    DATE_FORMATS = ["%d/%m/%Y", "%Y-%m-%d"]

    for date_format in DATE_FORMATS:
        try:
            return datetime.datetime.strptime(s, date_format)
        except ValueError:
            pass
        else:
            break

    return "Insira um valor válido"


@login_required(login_url='/accounts/login/')
#@permission_required('core.add_cardapio', raise_exception=True)
def cardapio(request):
    logado = False

    if request.user.is_active:
        logado = True

    if request.method == "POST":
        s = request.POST.get("campo")
        s = s.encode('utf8')

        campo = formata_data(s)

        if type(campo) == datetime.datetime:
            cardapios = Cardapio.objects.filter(data=campo)
            csrfContext = RequestContext(request)
            if cardapios:
                return render(request, 'cardapio/cardapios.html', {'cardapios': cardapios, 'logado': logado},
                              csrfContext)
            else:
                cardapios = Cardapio.objects.all()
                cardapios = paginador(request, cardapios)
                msg = "Nenhum registro encontrado"
                return render(request, 'cardapio/cardapios.html',
                              {'cardapios': cardapios, 'msg_err': msg, 'logado': logado})
        else:
            cardapios = Cardapio.objects.all()
            cardapios = paginador(request, cardapios)

            return render(request, 'cardapio/cardapios.html',
                          {'cardapios': cardapios, 'msg_err': campo, 'logado': logado})
    else:
        cardapios = Cardapio.objects.all()
        cardapios = paginador(request, cardapios)

        if '0' in request.session:
            msg = request.session['0']
            del request.session['0']
            return render(request, 'cardapio/cardapios.html', {'cardapios': cardapios, 'msg': msg, 'logado': logado})

        return render(request, 'cardapio/cardapios.html', {'cardapios': cardapios, 'logado': logado})


@login_required(login_url='/accounts/login/')
#@permission_required('core.add_cardapio', raise_exception=True)
def addCardapio(request):
    logado = False

    if request.user.is_active:
        logado = True

    if request.method == "POST":
        form = CardapioForm(request.POST)
        if form.is_valid():
            s = request.POST.get("data")
            s = s.encode('utf8')

            data = formata_data(s)

            cardapio = Cardapio(
                data=data,
                principal=request.POST.get("principal"),
                salada=request.POST.get("salada"),
                sobremesa=request.POST.get("sobremesa"),
                suco=request.POST.get("suco"),
            )

            cardapio.save()

            request.session[0] = "Cardápio inserido com sucesso"
            return redirect('/cardapio')
        else:
            return render(request, 'cardapio/add_cardapio.html', {'form': form, 'logado': logado})
    else:
        form = CardapioForm
        return render(request, 'cardapio/add_cardapio.html', {'form': form, 'logado': logado})


@login_required(login_url='/accounts/login/')
#@permission_required('core.change_cardapio', raise_exception=True)
def editarCardapio(request, id):
    logado = False

    if request.user.is_active:
        logado = True

    cardapio = get_object_or_404(Cardapio, pk=id)

    if request.method == 'POST':
        form = EditarCardapioForm(request.POST, initial={'id': id})
        if form.is_valid():
            s = request.POST.get("data")
            s = s.encode('utf8')

            data = formata_data(s)

            cardapio.data = data
            cardapio.principal = request.POST.get("principal")
            cardapio.salada = request.POST.get("salada")
            cardapio.sobremesa = request.POST.get("sobremesa")
            cardapio.suco = request.POST.get("suco")

            cardapio.save()

            cardapio = Cardapio.objects.all()
            cardapio = paginador(request, cardapio)

            request.session[0] = "Cardapio alterado com sucesso"
            return redirect('/cardapio')
        else:
            return render(request, 'cardapio/editar.html', {'form': form, 'logado': logado})
    else:
        form = EditarCardapioForm(initial={
            'data': cardapio.data,
            'principal': cardapio.principal,
            'salada': cardapio.salada,
            'sobremesa': cardapio.sobremesa,
            'suco': cardapio.suco
        })

        return render(request, 'cardapio/editar.html', {'form': form, 'logado': logado})


@login_required(login_url='/accounts/login/')
#@permission_required('core.delete_cardapio', raise_exception=True)
def deletarCardapio_confirm(request, id):
    qst = Cardapio.objects.filter(id=id)
    logado = False

    if request.user.is_active:
        logado = True

    for i in qst:
        cardapio = i
    return render(request, 'delete.html', {'cardapio': cardapio, 'app': 'Cardapio', 'logado': logado})


@login_required(login_url='/accounts/login/')
#@permission_required('core.delete_cardapio', raise_exception=True)
def deletarCardapio(request, id):
    obj = Cardapio.objects.filter(id=id)
    obj.delete()
    request.session[0] = "Cardapio deletado com sucesso"
    return redirect('/cardapio')