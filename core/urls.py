# -*- coding: utf-8 -*-

from django.conf.urls import url
from core import  views

urlpatterns = [
    url(r'^$', views.cardapio),
    url(r'^delete/(?P<id>[0-9]+)/$', views.deletarCardapio_confirm, name='deletar_cardapio'),
    url(r'^delete/pos_confirm/(?P<id>[0-9]+)/$', views.deletarCardapio, name='deletar_cardapio_posconfirm'),
    url(r'^edit/(?P<id>[0-9]+)/$', views.editarCardapio, name='editar_cardapio'),
    url(r'^add', views.addCardapio),
]
