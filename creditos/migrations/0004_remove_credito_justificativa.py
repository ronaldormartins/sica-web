# -*- coding: utf-8 -*-
# Generated by Django 1.11.29 on 2024-01-30 16:27
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('creditos', '0003_credito_data_criacao'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='credito',
            name='justificativa',
        ),
    ]
