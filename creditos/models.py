# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from aluno.models import Aluno
from django.db.models.fields import CharField
from django.conf import settings


class Historico(models.Model):
    data_criacao = models.DateTimeField(auto_now_add=True)
    justificativa = models.TextField()
    usuario = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    aluno = models.ForeignKey(Aluno, on_delete=models.CASCADE)
    unidades = models.IntegerField()
