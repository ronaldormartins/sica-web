# -*- coding: utf-8 -*-

from django.conf.urls import url
from creditos import views


urlpatterns = [
    url(r'^$', views.creditos),
    url(r'^add', views.addCredito, name="add_credito"),
    url(r'^historico/(?P<aluno_id>[0-9]+)/$', views.visualizar_historico, name='visualizar_historico'),
    url(r'^lista_alunos_historico/$', views.lista_alunos, name='lista_alunos_historico'),
]
