# -*- coding: utf-8 -*-
from django import forms

from aluno.models import Aluno

class CreditoForm(forms.ModelForm):

    justificativa = forms.CharField(required=False, widget=forms.TextInput(attrs={'class': 'form-control'}), label="Justificativa")
    aluno = forms.ModelChoiceField(queryset=Aluno.objects.filter(ativo=True).order_by("nome"), widget=forms.Select(attrs={'class': 'form-control'}), label="Aluno")
    credito_refeicao = forms.IntegerField(initial=None, required=False, widget=forms.NumberInput(attrs={'class': 'form-control'}), label="Crédito Refeição")

    class Meta:
        model = Aluno
        fields = ('credito_refeicao',)

    def __init__(self, *args, **kwargs):
        super(CreditoForm, self).__init__(*args, **kwargs)
        self.fields['credito_refeicao'].widget = forms.NumberInput(attrs={'class': 'form-control'})
        self.fields['aluno'].label_from_instance = lambda obj: "{} {}".format(obj.nome.encode('ascii', 'ignore'), obj.sobrenome.encode('ascii', 'ignore'))

    def clean(self):
        super(CreditoForm, self).clean()

        # Não é mais necessário verificar duplicatas aqui

        return self.cleaned_data