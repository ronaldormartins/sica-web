# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth.decorators import login_required, permission_required
from django.core.paginator import Paginator, EmptyPage
from django.db.models import Count
from django.shortcuts import render, redirect, get_object_or_404
from django.utils import timezone
from aluno.models import Aluno
from creditos.forms import CreditoForm
from creditos.models import Historico


@login_required(login_url='/accounts/login/')
#@permission_required('aluno.add_aluno', raise_exception=True)
def creditos(request):
    alunos_list = Aluno.objects.filter(credito_refeicao__gt=0).order_by('nome')
    logado = False

    if request.user.is_active:
        logado = True
    if request.method == "POST":
        campo = request.POST.get('campo')

        alunos_list = Aluno.objects.filter(nome__icontains=campo, credito_refeicao__gt=0)
        alunos = paginador(request, alunos_list)

        if alunos_list.__len__() == 0:
            alunos_list = Aluno.objects.filter(credito_refeicao__gt=0).order_by('nome')
            alunos = paginador(request, alunos_list)
            err = "Nenhum Registro encontrado"
            return render(request, 'credito/creditos.html', {'alunos': alunos, 'err': err, 'logado': logado})
    else:
        alunos = paginador(request, alunos_list)

        if '0' in request.session:
            msg = request.session['0']
            del request.session['0']
            return render(request, 'credito/creditos.html', {'alunos': alunos, 'msg': msg, 'logado': logado})

    return render(request, 'credito/creditos.html', {'alunos': alunos, 'logado': logado})


@login_required(login_url='/accounts/login/')
#@permission_required('aluno.add_aluno', raise_exception=True)
def addCredito(request):
    logado = request.user.is_active

    if request.method == "POST":
        form = CreditoForm(request.POST)
        if form.is_valid():
            aluno_id = request.POST.get("aluno")
            unidades_adicionadas = int(request.POST.get("credito_refeicao"))
            aluno = get_object_or_404(Aluno, pk=aluno_id)
            justificativa = request.POST.get("justificativa", "Atualização de créditos")



            aluno.credito_refeicao += unidades_adicionadas

            if  aluno.credito_refeicao < 0:
                form.add_error(None, "O novo crédito não pode ser negativo!")
                return render(request, 'credito/add_credito.html', {'form': form, 'logado': logado})
            else:
                aluno.save()
                Historico.objects.create(data_criacao=timezone.now(), justificativa=justificativa, usuario=request.user, aluno=aluno, unidades=unidades_adicionadas)
                request.session[0] = "Credito atualizado com sucesso"
                return redirect("/creditos")
        else:
            return render(request, 'credito/add_credito.html', {'form': form, 'logado': logado})
    else:
        form = CreditoForm()
        return render(request, 'credito/add_credito.html', {'form': form, 'logado': logado})

@login_required(login_url='/accounts/login/')
#@permission_required('aluno.add_aluno', raise_exception=True)
def lista_alunos(request):
    logado = False

    if request.user.is_active:
        logado = True

    if request.method == "POST":
        campo = request.POST.get('campo')

        #alunos_list = Aluno.objects.filter(nome__icontains=campo).order_by('nome', 'sobrenome')
        alunos_list = Aluno.objects.annotate(num_historicos=Count('historico')).filter(nome__icontains=campo, num_historicos__gt=0).order_by('nome', 'sobrenome')
        alunos = paginador(request, alunos_list)

        if alunos_list.__len__() == 0:
            #alunos_list = Aluno.objects.all().order_by('nome', 'sobrenome')
            alunos_list = Aluno.objects.annotate(num_historicos=Count('historico')).filter(num_historicos__gt=0).order_by('nome', 'sobrenome')

            alunos = paginador(request, alunos_list)
            err = "Nenhum Registro encontrado"
            return render(request, 'credito/lista_alunos_historico.html', {'alunos': alunos, 'err': err, 'logado': logado})
    else:
        #alunos_list = Aluno.objects.all().order_by('nome', 'sobrenome')
        alunos_list = Aluno.objects.annotate(num_historicos=Count('historico')).filter(num_historicos__gt=0).order_by('nome', 'sobrenome')
        alunos = paginador(request, alunos_list)
        return render(request, 'credito/lista_alunos_historico.html', {'alunos': alunos, 'logado': logado})

    return render(request, 'credito/lista_alunos_historico.html', {'alunos': alunos, 'logado': logado})

@login_required(login_url='/accounts/login/')
#@permission_required('aluno.add_aluno', raise_exception=True)
def visualizar_historico(request, aluno_id):
    logado = False

    if request.user.is_active:
        logado = True

    aluno = get_object_or_404(Aluno, pk=aluno_id)
    historicos = Historico.objects.filter(aluno=aluno).order_by('-data_criacao')

    return render(request, 'credito/historico_aluno.html', {
        'aluno': aluno,
        'historicos': historicos,
        'logado': logado
    })

def paginador(request, creditos_list):
    paginator = Paginator(creditos_list, 10)
    page = request.GET.get('page', 1)

    try:
        creditos = paginator.page(page)
    except EmptyPage:
        creditos = paginator.page(paginator.num_pages)
    except:
        creditos = paginator.page(1)

    return creditos