# -*- coding: utf-8 -*-
from django import forms
from django.contrib.auth.models import User


class CustomUserCreationForm(forms.ModelForm):
    senha = forms.CharField(label='Senha', widget=forms.PasswordInput(attrs={'class': 'form-control'}))
    is_active = forms.BooleanField(label='Ativo', initial=True, required=False)
    is_superuser = forms.BooleanField(label='Administrador', required=False)

    class Meta:
        model = User
        fields = ['username', 'email', 'senha', 'is_active', 'is_superuser']

    def __init__(self, *args, **kwargs):
        super(CustomUserCreationForm, self).__init__(*args, **kwargs)
        self.fields['username'].widget.attrs.update({'class': 'form-control'})
        self.fields['email'].widget.attrs.update({'class': 'form-control'})
        self.fields['senha'].widget.attrs.update({'class': 'form-control'})
        self.fields['username'].required = True
        self.fields['email'].required = True

    def save(self, commit=True):
        user = super(CustomUserCreationForm, self).save(commit=False)
        senha = self.cleaned_data["senha"]
        user.set_password(senha)
        user.is_active = self.cleaned_data["is_active"]
        user.is_superuser = self.cleaned_data["is_superuser"]
        if commit:
            user.save()
        return user


class UserEditForm(forms.ModelForm):
    senha = forms.CharField(label='Senha', widget=forms.PasswordInput(attrs={'class': 'form-control'}), required=False)
    is_active = forms.BooleanField(label='Ativo', required=False)
    is_superuser = forms.BooleanField(label='Administrador', required=False)  # Campo adicionado

    class Meta:
        model = User
        fields = ['username', 'email', 'is_active', 'is_superuser']

    def __init__(self, *args, **kwargs):
        super(UserEditForm, self).__init__(*args, **kwargs)
        self.fields['username'].widget.attrs.update({'class': 'form-control'})
        self.fields['email'].widget.attrs.update({'class': 'form-control'})
        self.fields['senha'].widget.attrs.update({'class': 'form-control'})
        self.fields['username'].required = True
        self.fields['email'].required = True

    def save(self, commit=True):
        user = super(UserEditForm, self).save(commit=False)
        senha = self.cleaned_data.get("senha")

        if senha:
            user.set_password(senha)
        user.is_superuser = self.cleaned_data.get("is_superuser", False)
        if commit:
            user.save()
        return user