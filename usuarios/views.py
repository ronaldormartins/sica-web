# -*- coding: utf-8 -*-
from django.contrib.auth.models import User
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required, permission_required
from django.core.paginator import Paginator, EmptyPage
from usuarios.forms import CustomUserCreationForm, UserEditForm

@login_required(login_url='/accounts/login/')
@permission_required('usuarios.add_user', raise_exception=True)
def list_users(request):
    logado = False

    if request.user.is_active:
        logado = True

    usuarios_list = User.objects.all().order_by('username')

    if request.method == "POST":
        campo = request.POST.get('campo')


        usuarios_list = User.objects.filter(curso__icontains=campo).order_by('username')
        usuarios = paginador(request, usuarios_list)

        if usuarios_list.__len__() == 0:
            cursos_list = User.objects.all().order_by('username')
            cursos = paginador(request, cursos_list)
            err = "Nenhum Registro encontrado"
            return render(request, 'usuarios/usuarios.html', {'cursos': cursos, 'err': err, 'logado': logado})
    else:
        usuarios = paginador(request, usuarios_list)

    return render(request, 'usuarios/usuarios.html', {'usuarios': usuarios, 'logado': logado})


@login_required(login_url='/accounts/login/')
@permission_required('usuarios.add_user', raise_exception=True)
def create_user(request):
    logado = False

    if request.user.is_active:
        logado = True

    if request.method == 'POST':
        form = CustomUserCreationForm(request.POST)
        if form.is_valid():
            form.save()

            request.session[0] = "Curso inserido com sucesso"

            return redirect('/usuarios/')
        else:
            return render(request, 'usuarios/add_usuario.html', {'form': form, 'logado': logado})
    else:
        form = CustomUserCreationForm()
    return render(request, 'usuarios/add_usuario.html', {'form': form, 'logado': logado})

@login_required(login_url='/accounts/login/')
@permission_required('usuarios.add_user', raise_exception=True)
def edit_user(request, id):
    user = get_object_or_404(User, pk=id)
    logado = False

    if request.user.is_active:
        logado = True

    if request.method == 'POST':
        form = UserEditForm(request.POST, instance=user)
        if form.is_valid():
            form.save()
            request.session[0] = "Curso alterado com sucesso"
            return redirect('/usuarios')
    else:
        form = UserEditForm(instance=user)
    return render(request, 'usuarios/edit_usuario.html', {'form': form, 'logado':logado, 'user_id': id})

@login_required(login_url='/accounts/login/')
@permission_required('usuarios.add_user', raise_exception=True)
def paginador(request, cursos_list):
    paginator = Paginator(cursos_list, 10)
    page = request.GET.get('page',1)
    
    try:
        cursos = paginator.page(page)
    except EmptyPage:
        cursos = paginator.page(paginator.num_pages)
    except:
        cursos = paginator.page(1)
    
    return cursos
