# -*- coding: utf-8 -*-

from django.conf.urls import url
from usuarios import  views

urlpatterns = [
    url(r'^$', views.list_users),
    url(r'^add', views.create_user),
    url(r'^edit/(?P<id>[0-9]+)/$', views.edit_user, name='editar_usuario'),
]