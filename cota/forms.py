# -*- coding: utf-8 -*-
from django import forms
from cota.models import Cota

class CotaForm(forms.ModelForm):

    class Meta:
        model = Cota
        fields = ('nome','quantidade',)
    
    def clean(self):
        super(CotaForm, self).clean()
        
        nome = self.cleaned_data.get('nome')
        cotas = Cota.objects.all()

        if self.errors:
            return self.cleaned_data
        
        for item in cotas:
            if item.nome.lower() == nome.lower():
                self.add_error('cota', u'Cota já cadastrado')

        return self.cleaned_data
    
class EditarCotaForm(forms.ModelForm):
    __ID = 0
    
    def __init__(self, *args, **kwargs):
        self.__ID = kwargs.pop('cota_id')
        super(EditarCotaForm, self).__init__(*args, **kwargs)


    class Meta:
        model = Cota
        fields = ('nome','quantidade')
    
    def clean(self):
        super(EditarCotaForm, self).clean()
        
        cota_id = int(self.__ID)
        cota = self.cleaned_data.get('nome')
        cotas = Cota.objects.all()
              
        if self.errors:
            return self.cleaned_data
        
        for item in cotas:
            if item.nome.lower() == cota.lower():
                if item.id != cota_id:
                    self.add_error('cota', u'Cota já cadastrado')
        
        return self.cleaned_data
