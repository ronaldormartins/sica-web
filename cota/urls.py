# -*- coding: utf-8 -*-

from django.conf.urls import url
from cota import views

urlpatterns = [
    url(r'^$', views.cotas),
    url(r'^delete/(?P<id>[0-9]+)/$', views.deletarCota_confirm, name='deletar_cota_posconfirm'),
    url(r'^delete/pos_confirm/(?P<id>[0-9]+)/$', views.deletar_cota, name='deletar_cota'),
    url(r'^add', views.addCota, name="add_cota"),
    url(r'^edit/(?P<id>[0-9]+)/$', views.editarCota, name='editar_cota'),
]
