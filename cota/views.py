from django.http import HttpResponse
from django.shortcuts import get_object_or_404, render, redirect
from django.template.context import RequestContext
from itertools import chain
from django.core.paginator import Paginator, EmptyPage
from django.contrib.auth.decorators import login_required, permission_required
from cota.forms import CotaForm, EditarCotaForm
from cota.models import Cota



@login_required(login_url='/accounts/login/')
#@permission_required('cota.add_cota', raise_exception=True)
def cotas(request):
    #return render(request, 'cota/cotas.html')
    cotas_list = Cota.objects.all()
    logado = False

    if request.user.is_active:
        logado = True
    if request.method == "POST":
        campo = request.POST.get('campo')

        cotas_list = Cota.objects.filter(nome__icontains=campo)
        cotas = paginador(request, cotas_list)

        if cotas_list.__len__() == 0:
            cotas_list = Cota.objects.all()
            cotas = paginador(request, cotas_list)
            err = "Nenhum Registro encontrado"
            return render(request, 'cota/cotas.html', {'cotas': cotas, 'err': err, 'logado': logado})
    else:
        cotas = paginador(request, cotas_list)

        if '0' in request.session:
            msg = request.session['0']
            del request.session['0']
            return render(request, 'cota/cotas.html', {'cotas': cotas, 'msg': msg, 'logado': logado})

    return render(request, 'cota/cotas.html', {'cotas': cotas, 'logado': logado})


@login_required(login_url='/accounts/login/')
#@permission_required('cota.add_cota', raise_exception=True)
def addCota(request):
    logado = False

    if request.user.is_active:
        logado = True
    if request.method == "POST":
        form = CotaForm(request.POST)
        if form.is_valid():
            cota = Cota(
                nome=request.POST.get("nome"),
                quantidade=request.POST.get("quantidade"),
            )

            cota.save()

            request.session[0] = "Cota inserido com sucesso"

            return redirect("/cotas")
        else:
            return render(request, 'cota/add_cota.html', {'form': form, 'logado': logado})
    else:
        form = CotaForm
        return render(request, 'cota/add_cota.html', {'form': form, 'logado': logado})


@login_required(login_url='/accounts/login/')
#@permission_required('cota.change_curso', raise_exception=True)
def editarCota(request, id):
    cota = get_object_or_404(Cota, pk=id)
    logado = False

    if request.user.is_active:
        logado = True
    if request.method == "POST":
        form = EditarCotaForm(cota_id=id, data=request.POST)
        if form.is_valid():
            cota.nome = request.POST.get("nome")
            cota.quantidade = request.POST.get("quantidade")

            cota.save()

            request.session[0] = "Cota alterado com sucesso"

            return redirect("/cotas")
        else:
            return render(request, 'cota/edit_cota.html', {'form': form, 'id': id, 'logado': logado})
    else:
        form = EditarCotaForm(initial={
            'nome': cota.nome,
            'quantidade': cota.quantidade,
        }, cota_id=id)

        return render(request, 'cota/edit_cota.html', {'form': form, 'id': id, 'logado': logado})

def paginador(request, cotas_list):
    paginator = Paginator(cotas_list, 10)
    page = request.GET.get('page', 1)

    try:
        cotas = paginator.page(page)
    except EmptyPage:
        cotas = paginator.page(paginator.num_pages)
    except:
        cotas = paginator.page(1)

    return cotas


@login_required(login_url='/accounts/login/')
#@permission_required('cota.delete_cota', raise_exception=True)
def deletarCota_confirm(request, id):
    logado = False

    if request.user.is_active:
        logado = True

    qst = Cota.objects.filter(id=id, ativo=True)
    for i in qst:
        cota = i
    return render('delete.html', {'cota': cota, 'app': 'Cota', 'logado': logado})


@login_required(login_url='/accounts/login/')
#@permission_required('cota.delete_cota', raise_exception=True)
def deletar_cota(request, id):
    '''obj = get_object_or_404(Cota, pk=id)
    obj.ativo = False
    obj.save()

    request.session[0] = "Cota deletada com sucesso"
    return redirect("/cotas")
    '''
    cota = get_object_or_404(Cota, pk=id)

    cota.delete()
    return redirect("/cotas")