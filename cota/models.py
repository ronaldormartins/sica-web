from __future__ import unicode_literals

from django.db import models
from aluno.models import Aluno

class Cota(models.Model):
    nome = models.CharField(max_length=100)
    quantidade = models.PositiveIntegerField()

    def __str__(self):
        return self.nome