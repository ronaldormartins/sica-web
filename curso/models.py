# -*- coding: utf-8 -*-

from django.db import models 
from django.db.models.fields import CharField
from cota.models import Cota

class Curso(models.Model):
    curso = CharField(max_length=255)
    ativo = models.BooleanField()
    cota = models.ForeignKey('cota.Cota', on_delete=models.SET_NULL, null=True, blank=True)


class Search(models.Model):
    campo = CharField(max_length=255) 
