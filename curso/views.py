# -*- coding: utf-8 -*-
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required, permission_required
from django.core.paginator import Paginator, EmptyPage
from curso.models import Curso
from curso.forms import CursoForm, EditarCursoForm

@login_required(login_url='/accounts/login/')
#@permission_required('curso.add_curso', raise_exception=True)
def cursos(request):
    cursos_list = Curso.objects.all()
    logado =  False
    
    if request.user.is_active:
        logado =  True
    if request.method == "POST":
        campo = request.POST.get('campo')
        
        cursos_list = Curso.objects.filter(curso__icontains=campo)
        cursos = paginador(request, cursos_list)
        
        if cursos_list.__len__() == 0:
            cursos_list = Curso.objects.all()
            cursos = paginador(request, cursos_list)
            err = "Nenhum Registro encontrado"
            return render(request, 'curso/cursos.html', {'cursos': cursos, 'err': err, 'logado': logado})
    else:
        cursos = paginador(request, cursos_list)
        
        if '0' in request.session:
            msg = request.session['0']
            del request.session['0']
            return render(request, 'curso/cursos.html', {'cursos': cursos, 'msg': msg, 'logado': logado})
        
    return render(request, 'curso/cursos.html', {'cursos': cursos, 'logado': logado})

@login_required(login_url='/accounts/login/')
#@permission_required('curso.add_curso', raise_exception=True)
def addCurso(request):
    logado =  False
    
    if request.user.is_active:
        logado =  True
    if request.method == "POST":
        form = CursoForm(request.POST)
        if form.is_valid():
            curso = Curso(
                curso = request.POST.get("curso"),
                ativo = request.POST.get("ativo"),
            )
            
            if curso.ativo == u'on':
                curso.ativo = True
            else:
                curso.ativo = False
            
            curso.save()
            
            request.session[0] = "Curso inserido com sucesso"
            
            return redirect("/cursos")
        else:
            return render(request, 'curso/add_curso.html', {'form': form, 'logado': logado})
    else:
        form = CursoForm
        return render(request, 'curso/add_curso.html', {'form': form, 'logado': logado})

@login_required(login_url='/accounts/login/')
#@permission_required('curso.change_curso', raise_exception=True)
def editarCurso(request, id):
    curso = get_object_or_404(Curso, pk = id)
    logado =  False
    
    if request.user.is_active:
        logado =  True
    if request.method == "POST":    
        form = EditarCursoForm(curso_id=id, data=request.POST)
        if form.is_valid():
            curso.curso = request.POST.get("curso")
            curso.ativo = request.POST.get("ativo")
            curso.cota = form.cleaned_data.get("cota")
            
            
            if curso.ativo == None:
                curso.ativo = False
	    if curso.ativo == u'on':
		curso.ativo = True
            
            curso.save()
            
            request.session[0] = "Curso alterado com sucesso"
            
            return redirect("/cursos")
        else:
            return render(request, 'curso/edit_curso.html', {'form': form, 'id': id, 'logado': logado})
    else:
        form = EditarCursoForm(initial={
            'curso': curso.curso,
            'ativo': curso.ativo,
            'cota': curso.cota,
        }, curso_id=id)

        return render(request, 'curso/edit_curso.html', {'form': form, 'id': id, 'logado': logado})

def paginador(request, cursos_list):
    paginator = Paginator(cursos_list, 10)
    page = request.GET.get('page',1)
    
    try:
        cursos = paginator.page(page)
    except EmptyPage:
        cursos = paginator.page(paginator.num_pages)
    except:
        cursos = paginator.page(1)
    
    return cursos
