# -*- coding: utf-8 -*-

from django.conf.urls import url
from curso import  views

urlpatterns = [
    url(r'^$', views.cursos),
    url(r'^add', views.addCurso),
    url(r'^edit/(?P<id>[0-9]+)/$', views.editarCurso, name='editar_curso'),
]