# -*- coding: utf-8 -*-

from django.contrib import admin
from curso.models import Curso
from curso.forms import CursoForm

class CursoAdmin(admin.ModelAdmin):
    list_display = ('id','curso','ativo')
    ordering = ('id','curso','ativo')
    search_fields = ('curso',)
    list_display_icons = True
    export_to_xls = True
    form = CursoForm
    
admin.site.register(Curso, CursoAdmin)


