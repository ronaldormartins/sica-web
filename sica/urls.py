# -*- coding: utf-8 -*-

from django.conf.urls import url, include
from django.contrib import admin
from core import views

admin.autodiscover()

urlpatterns = [
    url(r'^admin', include(admin.site.urls)),
    url(r'^home', views.home),
    url(r'^core/login', views.sica_login),
    url(r'^logout', views.sica_logout),
    url(r'^horarios/',  include('horario.urls')),
    url(r'^alunos/',  include('aluno.urls')),
    url(r'^cardapio/',  include('core.urls')),
    url(r'^cursos/',  include('curso.urls')),
    url(r'^relatorio/',  include('relatorio.urls')),
    url(r'^cotas/',  include('cota.urls')),
    url(r'^creditos/',  include('creditos.urls')),
    url(r'^$', views.home),
    url(r'accounts/login/', views.accountsLogin),
    url(r'accounts/password/', views.accountsPassword),
    url(r'^api/', include('api.urls')),
    url(r'^usuarios/',  include('usuarios.urls')),
]