# Para implementação deste sistema crie uma base de dados no Postgres e configure o arquivo de configuração no caminho especificado abaixo da seguinte forma:

sica_web -> sica -> settings.py

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'Nome do BD criado',
        'USER': 'Usuario para acessar o BD',
        'PASSWORD': 'Senha do Usuario',
        'HOST': '127.0.0.1',
        'PORT': '5432',
    },
}
